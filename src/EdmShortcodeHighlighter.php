<?php namespace ESH;

/**
 * This class is used to Add user roles
 */
class EdmShortcodeHighlighter {

	private $background_color = '#0e2961';
	private $border_color = '#a1dd00';
	private $text_color = '#a1dd00';
	private $parameter_color = '#006EFF';

	public function __construct()
	{

	}

	public function init_action()
	{
		add_filter( 'mce_css', array($this, 'wpsh_custom_mce_style' ));
		add_action( 'admin_init', array($this, 'wpsh_admin_init') );
		add_filter( 'wp_insert_post_data', array($this, 'wpsh_remove_markup'), '99', 2 );
		add_action( 'wp_ajax_wpsh_dynamic_styles', array($this, 'wpsh_dynamic_styles_callback'));
	}

	public function wpsh_custom_mce_style( $mce_css )
	{
		if ( ! empty( $mce_css ) )
			$mce_css .= ',';
		$mce_css .= EDM_SHORTCODE_HIGH_DIR_URL . 'assets/wp-shortcode-highlighter-editor.css' . ',' . admin_url('admin-ajax.php') ."/?action=wpsh_dynamic_styles";
		return $mce_css;
	}

	public function wpsh_admin_init()
	{
		add_filter( 'mce_external_plugins', array($this, 'wpsh_custom_mce_script') );
		wp_enqueue_style( 'wpsh-style', EDM_SHORTCODE_HIGH_DIR_URL . 'assets/wp-shortcode-highlighter-editor.css');
	}

	public function wpsh_custom_mce_script( $plugin_array )
	{
		$plugin_array['shortcode_highlighter'] = EDM_SHORTCODE_HIGH_DIR_URL . 'assets/wp-shortcode-highlighter-editor.js' ;
		return $plugin_array;
	}


	public function wpsh_dynamic_styles_callback()
	{
		header("Content-Type: text/css");
		header("X-Content-Type-Options: nosniff");
		echo "#tinymce span.shortcode-highlighter-parameter {color:".esc_attr( $this->parameter_color ).";}";
		echo "#tinymce span.shortcode-highlighter-container {background-color:".esc_attr( $this->background_color )."; border-color:".esc_attr( $this->border_color )."; color:".esc_attr( $this->text_color ).";}";
	}

	public function wpsh_remove_markup( $data , $postarr )
	{
		$dom = new \DOMDocument();
		$dom->preserveWhiteSpace = true;
		@$dom->loadHTML('<div>' . utf8_decode(stripslashes($data['post_content'])) . '</div>');
		$xpath = new \DomXPath($dom);
		$classname='shortcode-highlighter';
		$xpath_results = $xpath->query("//span[contains(@class, $classname)]");
		foreach($xpath_results as $span) {
			$possibillitiesStart = array('"',"'",'[');
			$possibillitiesEnd = array('"',"'",']');
			if(in_array(substr($span->textContent,0,1),$possibillitiesStart) && in_array(substr($span->textContent,-1),$possibillitiesEnd)){
				while($span->hasChildNodes()) {
					$span->parentNode->insertBefore($span->firstChild, $span);
				}
				$span->parentNode->removeChild($span);
			}
		}
		$html  = $dom->saveHTML();
		$html = html_entity_decode($html);
		$start = strpos($html, '<div>');
		$end   = strrpos($html, '</div>');
		$data['post_content'] = addslashes( substr($html, $start + 5, $end - $start - 5));
		return $data;
	}

}

