<?php
/*
Plugin Name: EDM Shortcode Highlighter
Plugin URI: https://wordpress.org/plugins/wp-shortcode-highlighter/
Description: Highlights the shortcodes in the editor so they are easier to see. (Based from the actual plugin made by Roland Murg).
Author: EDM
Version: 1.0
*/

if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once('vendor/autoload.php');

$dir = dirname( __FILE__ );

define('EDM_SHORTCODE_HIGH_DIR_URL',plugin_dir_url( __FILE__ ) );
define('EDM_SHORTCODE_HIGH_DIR_PATH',plugin_dir_path( __FILE__ ) );
define('EDM_SHORTCODE_HIGH_BASENAME',plugin_basename( __FILE__ ) );

(new ESH\EdmShortcodeHighlighter)->init_action();